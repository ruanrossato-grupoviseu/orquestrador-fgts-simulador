# IMPORT SELENIUM
# from selenium import webdriver
# from selenium.webdriver.common.by import By
# from selenium.webdriver.remote.webelement import WebElement
import time

# # IMPORT FUNCOES
# import src.funcoes.file_folder as ff
# import src.funcoes.api_formatacao as af
# import src.funcoes.pdf as pdf
# import src.funcoes.regex as regex

# IMPORT COMPONENTES
import src.components.browse as browse
# import src.components.lista as lista
# import src.components.aguardar as aguardar
# import src.components.clicar as clicar
# import src.components.inserir_texto as inserir_texto
# import src.components.buscar as buscar
# import src.components.frame as frame
# import src.components.window_handles as window_handles

# # IMPORT CLASS
# from src.models.ApiViseu import ApiViseu
# from src.models.Database import Database

# IMPORT SISTEMAS
from src.sistemas.consigc6 import Consigc6
from src.sistemas.safra import Safra

def startBotC6(userInfo, simulation = True):
    try:
        driver = browse.get_driver()
    except Exception as error:
        print(f"Erro: {error}")
        raise Exception("Error getting driver")
        
    
    consigc6 = Consigc6(driver)

    consigc6.abrir_site()

    consigc6.login()

    menu_xpath = "/html/body/div[3]/nav/div/ul/li[1]/a"
    consigc6.clicar_segurar_menu(menu_xpath)
    time.sleep(1)

    menu_xpath = "/html/body/div[3]/nav/div/ul/li[1]/ul/li[1]/a"
    consigc6.clicar_menu(menu_xpath)

    consigc6.clicar_digital()
    
    consigc6.consultar(userInfo["cpf"])

    consigc6.obter_saldo()

    menu_xpath = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[3]/div[2]/div/div[1]/div[2]/div[1]/select"
    consigc6.clicar_segurar_menu(menu_xpath)

    consigc6.clicar_saque_total()
    
    results = ""
    if(simulation):
        results = consigc6.mensagem_retorno()
    
    if(not simulation):
        results = consigc6.enviar_proposta(userInfo)
    
    consigc6.logout()

    return(results)

def startBotSafra(userInfo, simulation = True):
    driver = browse.get_driver()

    safra = Safra(driver)

    safra.abrir_site()

    safra.login()

    safra.clicar_painel_de_controle()

    safra.clicar_proposta()

    safra.agente_certificado()

    safra.consultar(userInfo["cpf"])

    results = ""
    if(simulation):
        results = safra.obter_saldo()

    safra.logout()
    return(results)

if(__name__=="__main__"):
    startBotSafra("45875076879","whatsapp:+992448799")
    startBotC6("45875076879","whatsapp:+992448799")

