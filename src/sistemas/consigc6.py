import src.components.browse as browse
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import WebDriver
import time
import src.components.aguardar as aguardar
import src.components.inserir_texto as inserir_texto
import src.components.clicar as clicar
import src.components.lista as lista
import src.components.buscar as buscar
import src.components.mouse as mouse
import src.components.limpar as limpar
from selenium.webdriver.common.keys import Keys
import urllib3
import json


class Consigc6:
    def __init__(self, driver: WebDriver, url: str = "https://c6.c6consig.com.br/WebAutorizador/MenuWeb/Esteira/AprovacaoConsulta/UI.AprovacaoConsultaAnd.aspx?FISession=f2eb986bd83a"):
        self.url = url
        self.driver = driver

    def abrir_site(self, ):
        browse.navegarSite(self.driver, self.url)

    def login(self):
        #DIGITAR LOGIN
        elemento = '/html/body/form/div[3]/table[2]/tbody/tr/td[10]/table/tbody/tr/td[1]/input'
        timeout = 10
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        elemento_texto = "48809315863_000058"
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, elemento_texto)

        #DIGITAR SENHA
        elemento = '/html/body/form/div[3]/table[2]/tbody/tr/td[14]/table/tbody/tr/td[1]/input'
        timeout = 10
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        elemento_texto = "@Bueno-873"
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, elemento_texto)

        #CLICAR EM ENTRAR
        elemento = '/html/body/form/div[3]/table[2]/tbody/tr/td[16]/a'
        timeout = 10
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(2)
        try:
            #VERIFICAR CAIXA DE ALERTA 
            assert self.driver.switch_to.alert.text == "Usuário já autenticado em outra estação. Deseja desconectar-se da estação e conectar-se através desta?"
            #CLICAR EM OK
            self.driver.switch_to.alert.accept()
        except Exception:
            pass

    def clicar_menu(self, xpath:str):
        elemento = buscar.busca_elemento_por_xpath(self.driver, xpath)

        mouse.clicar_elemento(self.driver, elemento)
        try:
            #VERIFICAR CAIXA DE ALERTA 
            assert self.driver.switch_to.alert.text == "É obrigatório selecionar o tipo de formalização"
            #CLICAR EM OK
            self.driver.switch_to.alert.accept()
        except Exception:
            pass

    def clicar_segurar_menu(self, xpath:str):
        elemento = buscar.busca_elemento_por_xpath(self.driver, xpath)

        mouse.clicar_segurar_elemento(self.driver, elemento)
        
    def clicar_digital(self):
        elemento = '/html/body/form/div[3]/table/tbody/tr/td/div[1]/table/tbody/tr[2]/td/div/table/tbody/tr[2]/td/table/tbody/tr/td[1]/fieldset/table/tbody/tr/td[2]/input'
        timeout = 10
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
    
    def consultar(self,cpf=""):
        time.sleep(1)
        #INSERIR CPF
        elemento = '/html/body/form/div[3]/table/tbody/tr/td/div[1]/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td/div/div/div[1]/div[2]/input'
        elemento_texto = cpf
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, elemento_texto)
        time.sleep(1)
        #INSERIR NOME
        elemento = '/html/body/form/div[3]/table/tbody/tr/td/div[1]/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td/div/div/div[1]/div[3]/input'
        timeout = 10
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout).clear()
        time.sleep(2)
        elemento_texto = 'Simulação teste'
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, elemento_texto)
        time.sleep(1)
        #INSERIR DATA NASCIMENTO
        elemento = '/html/body/form/div[3]/table/tbody/tr/td/div[1]/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td/div/div/div[1]/div[4]/input'
        elemento_texto = '14041912'
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, elemento_texto)
        time.sleep(1)
        #CLICAR EM CONFIRMAR
        elemento = '/html/body/form/div[3]/table/tbody/tr/td/div[1]/table/tbody/tr[4]/td/div/table/tbody/tr[2]/td/div/table/tbody/tr/td[1]/span/div[1]/div[2]/div[2]/table/tbody/tr/td/a'
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
   
    def obter_saldo(self):
        #CLICAR EM OBTER SALDO
        elemento = '/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div/div[2]/div/div[4]/div/div/table/tbody/tr/td/div/table[1]/tbody/tr/td[4]/span/div[1]/div[2]/div[2]/table/tbody/tr/td/a'
        timeout = 10
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        try:
            if("Fiduciária" in self.driver.switch_to.alert.text):
                print(self.driver.switch_to.alert.text)
                self.driver.switch_to.alert.accept()
                enviar_video_autorizar = "Enviar video aceitar consulta por outro banco"
                print(f"{enviar_video_autorizar} enviado")
                elemento = '/html/body/form/table[2]/tbody/tr/td[3]/a'
                clicar.clicar_elemento_por_xpath(self.driver, elemento)

            if("não possui adesão ao saque aniversário" in self.driver.switch_to.alert.text ):
                print(self.driver.switch_to.alert.text)
                self.driver.switch_to.alert.accept()
                enviar_video_adesao = "Enviar video adesão saque aniversário"
                print(f"{enviar_video_adesao} enviado")
                elemento = '/html/body/form/table[2]/tbody/tr/td[3]/a'
                clicar.clicar_elemento_por_xpath(self.driver, elemento)
        except:
            pass
        #CLICAR EM TABELA
        elemento = '/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[3]/div[2]/div/div[1]/div[1]/div[1]/input'
        timeout = 10
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        elemento_texto = '0001'
        time.sleep(1)
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, elemento_texto)
            
            
    def clicar_saque_total(self):
        time.sleep(2)
        try:
            elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[3]/div[2]/div/div[1]/div[2]/div[1]/select/option[2]"
            timeout = 10
            aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
            clicar.clicar_elemento_por_xpath(self.driver, elemento)
            time.sleep(3)
            #CLICAR EM CALCULAR
            elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[3]/div[2]/div/div[1]/div[2]/div[3]/span/div[1]/div[2]/div[2]/table/tbody/tr/td/a"
            timeout = 10
            aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
            clicar.clicar_elemento_por_xpath(self.driver, elemento)
        except Exception:
            self.driver.close()
   

    def mensagem_retorno(self):
        time.sleep(1)
        try:
            #VERIFICAR CAIXA DE ALERTA 
            if("valor mínimo: 300" in self.driver.switch_to.alert.text):
                print("valor mínimo")
                #CLICAR EM OK
                self.driver.switch_to.alert.accept()
                elemento = '/html/body/form/table[2]/tbody/tr/td[3]/a'
                clicar.clicar_elemento_por_xpath(self.driver, elemento)
                print("Saldo insuficiente, valor mínimo permitido R$ 300,00")
        except Exception:
            pass
        try:
            elemento = '/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[3]/div[2]/div/div[2]/div/div[2]/table[1]/tbody/tr/td/div/table/tbody/tr[2]/td[2]/span'
            saldo = buscar.busca_elemento_texto_por_xpath(self.driver, elemento)
            
            # print(f"Saldo disponível para saque R$: {saldo}")
            time.sleep(2)
            elemento = '/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[3]/div[2]/div/div[2]/div/div[2]/table[2]/tbody/tr/td/div/table/tbody/tr[3]/td[4]'
            valorRecebido = buscar.busca_elemento_texto_por_xpath(self.driver, elemento)
            # print(f"Você receberá R$: {valorRecebido} ")
            # print("taxa de juros(ao mês): 1,99% \nParcelas: Desconto direto no saldo FGTS")
            print(f"Saldo:{saldo}\nValor:{valorRecebido}")
            return {"balance":saldo,"value":valorRecebido}
        except Exception as err:
            print(err)
    
    def enviar_proposta(self, userInfo):
        #INSERIR NOME COMPLETO
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[2]/div[1]/input"
        # nome_completo = "RAPHAEL SAMED NAKHOUL"
        limpar.limpar_campo(self.driver, elemento, userInfo["name"])
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, userInfo["name"])

        #SELECIONAR GÊNERO
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[2]/div[3]/select"
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        
        if(userInfo["gender"].upper() == "M"):
            elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[2]/div[3]/select/option[2]"
            clicar.clicar_elemento_por_xpath(self.driver, elemento)
        elif(userInfo["gender"].upper() == "F"):
            elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[2]/div[3]/select/option[3]"
            clicar.clicar_elemento_por_xpath(self.driver, elemento)

        #SELECIONAR ESTADO CIVIL
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[2]/div[4]/select"
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[2]/div[4]/select/option[5]"
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        
        #SELECIONAR TIPO DOCUMENTO
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[3]/div[1]/select"
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[3]/div[1]/select/option[2]"
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)

        #INSERIR RG
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[3]/div[2]/input"
        # rg = "439914085"
        limpar.limpar_campo(self.driver, elemento, userInfo["id_number"])
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, userInfo["id_number"])

        time.sleep(1)
        #SELECIONAR UF
        elemento = '/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[3]/div[4]/select'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        # uf = "SP"
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, userInfo["state"])
        time.sleep(1)
        self.driver.find_element(By.XPATH, elemento).send_keys(Keys.RETURN)
        time.sleep(1)

        #INSERIR DATA EMISSÃO
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[3]/div[5]/input"
        # data_emissao = "20102010"
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, userInfo["id_date"].replace("/","").replace(".",""))
        time.sleep(1)

        #INSERIR NOME DA MÃE
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[4]/div/input"
        # nome_mae = "ROSELI SAMED NAKHOUL"
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, userInfo["mother_name"])
        time.sleep(1)

        #INSERIR DDD
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[5]/div[1]/input"
        # ddd = "11"
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, userInfo["phoneNumber"].split("+")[1][2:4])
        time.sleep(1)

        #INSERIR TELEFONE
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[5]/div[2]/input"
        # telefone = "972913993"
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, userInfo["phoneNumber"].split("+")[1][4:])
        time.sleep(1)

        #INSERIR RENDA
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[2]/div/div[5]/div[5]/input"
        # renda = "2200,00"
        limpar.limpar_campo(self.driver, elemento, userInfo["income"])
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, userInfo["income"])
        time.sleep(1)

        #INSERIR CEP
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[4]/div/div[1]/div[1]/input"
        # cep = "03071-080"
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, userInfo["zip_code"])
        time.sleep(2)

        #INSERIR NÚMERO RESIDENCIA
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[4]/div/div[1]/div[3]/input"
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(2)
        # numero_cep = "252"
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, userInfo["address_number"])
        time.sleep(1) 

        #INSERIR BANCO
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[6]/div/div/div[1]/input"
        # banco = "033"
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, userInfo["bank_number"])
        time.sleep(1)   

        #INSERIR AGÊNCIA
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[6]/div/div/div[2]/input"
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        # agencia = "0562"
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, userInfo["bank_agency_number"])
        time.sleep(1)   

        #INSERIR CONTA SEM DÍGITO
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[6]/div/div/div[3]/input"
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        # conta = "01031424"
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, userInfo["bank_account_number"])
        time.sleep(1)

        #INSERIR DÍGITO
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[6]/div/div/div[4]/input"
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        # digito = "6"
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, userInfo["bank_account_last_digit"])
        time.sleep(1)

        #SELECIONAR TIPO DE CONTA
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[6]/div/div/div[5]/select"
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        tipo_conta = "corrente".upper()
        if(userInfo["bank_account_type"] == "CORRENTE" ):
            elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[6]/div/div/div[5]/select/option[3]"
            clicar.clicar_elemento_por_xpath(self.driver, elemento)
        if(userInfo["bank_account_type"] == "POUPANCA"):
            elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[5]/div[1]/div/div[6]/div/div/div[5]/select/option[5]"
            clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)

        #INSERIR CPF AGENTE
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[13]/div[2]/div/div[1]/table/tbody/tr[2]/td/table/tbody/tr/td[1]/input"
        cpf_agente = "44303403822"
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, cpf_agente)
        time.sleep(1)
        #SELECIONAR DIGITADOR
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[13]/div[2]/div/div[1]/table/tbody/tr[2]/td/table/tbody/tr/td[3]/select"
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[13]/div[2]/div/div[1]/table/tbody/tr[2]/td/table/tbody/tr/td[3]/select/option[2]"
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        # #CLICAR EM ENVIAR PROPOSTA
        # elemento = "/html/body/form/div[3]/table/tbody/tr/td/div[1]/div/div[1]/div[2]/div[15]/div/span[1]/div[1]/div[2]/div[2]/table/tbody/tr/td/a"
        # clicar.clicar_elemento_por_xpath(self.driver, elemento)
        print("Proposta enviada com sucesso")

    def logout(self):
        elemento = '/html/body/form/table[2]/tbody/tr/td[3]/a'
        clicar.clicar_elemento_por_xpath(self.driver, elemento)




