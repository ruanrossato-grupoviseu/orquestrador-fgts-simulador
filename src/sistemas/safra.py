import src.components.browse as browse
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import WebDriver
import time
import src.components.aguardar as aguardar
import src.components.inserir_texto as inserir_texto
import src.components.clicar as clicar
import src.components.lista as lista
import src.components.buscar as buscar
import src.components.mouse as mouse



class Safra:
    def __init__(self, driver: WebDriver, url: str = "https://epfweb.safra.com.br/PainelControle#"):
        self.url = url
        self.driver = driver

    def abrir_site(self, ):
        browse.navegarSite(self.driver, self.url)

    def login(self):
        #DIGITAR LOGIN
        elemento = '/html/body/div[2]/div[1]/form/div/div/table/tbody/tr/td/div/input[1]'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        elemento_texto = "CBSU72508"
        inserir_texto.inserir_texto_por_letra_xpath(self.driver, elemento, elemento_texto)
        #DIGITAR SENHA
        elemento = '/html/body/div[2]/div[1]/form/div/div/table/tbody/tr/td/div/input[2]'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        elemento_texto = "@Brage1536"
        inserir_texto.inserir_texto_por_letra_xpath(self.driver, elemento, elemento_texto)
        #CLICAR EM ENTRAR
        elemento = '/html/body/div[2]/div[1]/form/div/div/table/tbody/tr/td/div/div/input'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        # try:
        #     #VERIFICAR CAIXA DE ALERTA 
        #     assert self.driver.switch_to.alert.text == "Usuário já autenticado em outra estação. Deseja desconectar-se da estação e conectar-se através desta?"
        #     #CLICAR EM OK
        #     self.driver.switch_to.alert.accept()
        # except Exception:
        #     pass

    def clicar_painel_de_controle(self):
        elemento = '/html/body/div[2]/div[2]/div/span/a[1]'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)

    def clicar_proposta(self):
        elemento = '/html/body/div[3]/div[2]/div[1]/div[1]/table/tbody/tr/td/div/form/p/button[2]'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        
    def agente_certificado(self):
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[1]/fieldset/div[2]/p[3]/input[1]'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        elemento_texto = "000000"
        inserir_texto.inserir_texto_por_xpath(self.driver, elemento, elemento_texto)
        elemento = '/html/body/ul[2]/li[2]/a'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        #CLICAR EM PROSSEGUIR
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[1]/fieldset/div[2]/button'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
    
    def consultar(self,cpf):
        time.sleep(1)
        #SELECIONAR FGTS
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/p[2]/div/a'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/p[2]/div/div/ul/li[7]'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        #SELECIONAR NOVO
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/p[3]/div/a'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/p[3]/div/div/ul/li[2]'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        #INSERIR CPF
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/p[4]/input'
        elemento_texto = cpf
        inserir_texto.inserir_texto_por_letra_xpath(self.driver, elemento, elemento_texto)
        time.sleep(1)
        #INSERIR NOME
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/p[5]/input'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        elemento_texto = 'Simular Teste'
        inserir_texto.inserir_texto_por_letra_xpath(self.driver, elemento, elemento_texto)
        time.sleep(1)
        #SELECIONAR TIPO DE PRODUTO
        elemento = "/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/div[2]/p/div/a"
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        elemento = "/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/div[2]/p/div/div/ul/li[2]"
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        #SELECIONAR DIGITAL FGTS 
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/div[5]/p/div/a/span'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/div[5]/p/div/div/ul/li[2]'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        #INSERIR DDD
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/div[6]/p/input[1]'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        elemento_texto = '11'
        inserir_texto.inserir_texto_por_letra_xpath(self.driver, elemento, elemento_texto)
        time.sleep(1)
        #INSERIR TELEFONE 
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/div[6]/p/input[2]'
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/div[6]/p/input[2]'
        elemento_texto = '912345678'
        inserir_texto.inserir_texto_por_letra_xpath(self.driver, elemento, elemento_texto)
        time.sleep(1)
        #SELECIONAR UF 
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/div[8]/p/div/a'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/div[8]/p/div/div/ul/li[26]'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(1)
        #CLICAR EM PROSSEGUIR 
        elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[2]/div/fieldset/div/button/span'
        timeout = 180
        aguardar.aguardar_elemento_ser_clicavel_xpath(self.driver, elemento, timeout)
        clicar.clicar_elemento_por_xpath(self.driver, elemento)
        time.sleep(2)

    def obter_saldo(self):
        try:
            #CLICAR EM OBTER SALDO
            elemento = ".toast-message"
            elemento_texto = buscar.busca_elemento_texto_por_classe_selector(self.driver, elemento)
            if("Instituição Fiduciária" in elemento_texto):
                # print(elemento_texto)
                elemento = '/html/body/div[2]/div[3]/a'
                clicar.clicar_elemento_por_xpath(self.driver, elemento)
                
                           
        except Exception:
            #CLICAR EM TABELA
            elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[5]/div/fieldset[1]/div/table/tbody/tr[1]/td[2]'
            elemento_texto = buscar.busca_elemento_texto_por_xpath(self.driver, elemento).split(" ")
            elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[5]/div/fieldset[1]/div/table/tbody/tr[1]/td[3]/input[1]'
            elemento_text = elemento_texto[1]
            inserir_texto.inserir_texto_por_letra_xpath(self.driver, elemento, elemento_text)
            valor1 = float(elemento_text.replace(".", "").replace(",", "."))
            # print(valor1)
            elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[5]/div/fieldset[1]/div/table/tbody/tr[2]/td[2]'
            elemento_texto = buscar.busca_elemento_texto_por_xpath(self.driver, elemento).split(" ")
            elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[5]/div/fieldset[1]/div/table/tbody/tr[2]/td[3]/input[1]'
            elemento_text = elemento_texto[1]
            inserir_texto.inserir_texto_por_letra_xpath(self.driver, elemento, elemento_text)
            valor2 = float(elemento_text.replace(".", "").replace(",", "."))
            # print(valor2)
            elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[5]/div/fieldset[1]/div/table/tbody/tr[3]/td[2]'
            elemento_texto = buscar.busca_elemento_texto_por_xpath(self.driver, elemento).split(" ")
            elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[5]/div/fieldset[1]/div/table/tbody/tr[3]/td[3]/input[1]'
            elemento_text = elemento_texto[1]
            inserir_texto.inserir_texto_por_letra_xpath(self.driver, elemento, elemento_text)
            valor3 = float(elemento_text.replace(".", "").replace(",", "."))
            # print(valor3)
            elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[5]/div/fieldset[1]/div/table/tbody/tr[4]/td[2]'
            elemento_texto = buscar.busca_elemento_texto_por_xpath(self.driver, elemento).split(" ")
            elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[5]/div/fieldset[1]/div/table/tbody/tr[4]/td[3]/input[1]'
            elemento_text = elemento_texto[1]
            inserir_texto.inserir_texto_por_letra_xpath(self.driver, elemento, elemento_text)
            valor4 = float(elemento_text.replace(".", "").replace(",", "."))
            # print(valor4)
            elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[5]/div/fieldset[1]/div/table/tbody/tr[5]/td[2]'
            elemento_texto = buscar.busca_elemento_texto_por_xpath(self.driver, elemento).split(" ")
            elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[5]/div/fieldset[1]/div/table/tbody/tr[5]/td[3]/input[1]'
            elemento_text = elemento_texto[1]
            inserir_texto.inserir_texto_por_letra_xpath(self.driver, elemento, elemento_text)
            valor5 = float(elemento_text.replace(".", "").replace(",", "."))
            # print(valor5)
            valor_total = valor1 + valor2 + valor3 + valor4 + valor5
            # print(valor_total)
            time.sleep(2)
            
            #SELECIONAR TAXA
            elemento = "//div[@id='IdTabelaJuros_chzn']/a/div/b"
            clicar.clicar_elemento_por_xpath(self.driver, elemento)
            
            elemento = "/html/body/div[3]/div[2]/div[1]/div[3]/div[5]/div/fieldset[2]/div[1]/div[2]/div/p/div/div/ul/li[3]"
            elemento_web = buscar.busca_elemento_por_xpath(self.driver, elemento)
            clicar.clicar_elemento_por_elemento_mouse(self.driver, elemento_web)
            #CLICAR EM SIMULAR
            elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[5]/div/fieldset[2]/button[1]/span'
            clicar.clicar_elemento_por_xpath(self.driver,elemento)

            time.sleep(1)
            # print(f"Saldo disponível para saque R$: {valor_total}")
            time.sleep(2)
            elemento = '/html/body/div[3]/div[2]/div[1]/div[3]/div[5]/div/fieldset[2]/div[4]/div[1]/div/div[3]/div[3]/div/table/tbody/tr[2]/td[6]'
            valor_receber= buscar.busca_elemento_texto_por_xpath(self.driver, elemento)
            # print(f"Você receberá R$: {valor_receber} ")
            # print("taxa de juros(ao mês): 1,99% \nParcelas: Desconto direto no saldo FGTS")

            currency = "{:,.2f}".format(valor_total)
            main_currency, fractional_currency = currency.split(".")[0], currency.split(".")[1]
            new_main_currency = main_currency.replace(",", ".")
            valor_total = new_main_currency + "," + fractional_currency
            print(f"Saldo:{valor_total}\nValor:{valor_receber}")
            return {"balance":valor_total,"value":valor_receber}
    # def safra_enviar_proposta(self):

       
    def logout(self):
       elemento = '/html/body/div[2]/div[3]/a'
       clicar.clicar_elemento_por_xpath(self.driver, elemento)




