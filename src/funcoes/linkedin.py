def find_linkedin_url(value:str) -> str:
    linkedin_url = 'https://www.linkedin.com/in/'

    # LOCALIZAR POSICAO INICIAL DO LINK DO LINKEDIN 
    linkedin_url_position = value.find(linkedin_url)

    # print("Posicao localizada url: ", linkedin_url_position)
    # SE LOCALIZOU POSICAO INICIAL
    if(linkedin_url_position >= 0):
        # BUSCAR TAMANHO LINK DO LINKEDIN 
        linkedin_url_len = len(linkedin_url)

        linkedin_url_position += linkedin_url_len
        # REMOVER LINK DO LINKEDIN DA MENSAGEM E DAR TRIM 
        linkedin_url_profile = value[linkedin_url_position:].strip()
        # print("Texto pós linkedin url: ", linkedin_url_profile)
        # LOCALIZAR UMA QUEBRA DE LINHA NA MENSAGEM 
        linkedin_url_position = linkedin_url_profile.find('\n')

        # SE LOCALIZOU QUEBRA DE LINHA 
        if(linkedin_url_position >= 0):
            # REMOVER TUDO PÓS QUEBRA DE LINHA => SOBRA O PERFIL
            linkedin_url_profile = linkedin_url_profile[:linkedin_url_position].strip()
            
    # CONCATENAR E DEVOLVER 
    return f"{linkedin_url}/{linkedin_url_profile}"
        