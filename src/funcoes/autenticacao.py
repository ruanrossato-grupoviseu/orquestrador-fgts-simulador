#from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.ie.webdriver import WebDriver
import keyring


def login_linkedin(driver: WebDriver):
    
    username = "usuario"
    senha = "senha"
    credential_key = "viseu_selenium_linkedin"

    # COMENTAR LINHA ABAIXO PÓS USO 
    keyring.set_password(credential_key, username, senha)

    # USUARIO
    input_usuario = driver.find_element(By.ID, "username")
    input_usuario.send_keys(username)

    # SENHA
    input_usuario = driver.find_element(By.ID, "password")
    input_usuario.send_keys(keyring.get_password(credential_key, username) )

    # CLICA BOTAO LOGIN
    driver.find_element(By.CLASS_NAME, "btn__primary--large").click()