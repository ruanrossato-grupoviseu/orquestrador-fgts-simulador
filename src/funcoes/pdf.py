import fitz


def read_pdf(file:str) -> str:

    file_text = ""

    with fitz.open(file) as doc:
        for page in doc:
            file_text += page.getText()

    return file_text

