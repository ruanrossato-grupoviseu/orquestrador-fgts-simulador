import pymssql

class Database:
    def __init__(
        self,
        environment_prod = False
    ):
        # COMENTAR LINHA ABAIXO PÓS USO 
        # keyring.set_password("viseu_database_dev", "sa", "Viseu2020")
        self.environment_prod = environment_prod
        self.database_username = "sa"
        # self.database_pass = keyring.get_password(credential_key, self.database_username) 
        self.database_pass = "Viseu2020"
        print(self.database_pass)
        if(self.environment_prod == False):
            self.database_server_name = "database-1.cmhllh4fzwy9.us-east-1.rds.amazonaws.com"
            self.database_name = "ViseuInnovation_Dev"

        if(self.environment_prod == True):
            self.database_server_name = "prod.cqfc8nzn9vmn.us-east-1.rds.amazonaws.com"
            self.database_name = "portal"
            

    def connect(self):
        self.conn = pymssql.connect(
            server=self.database_server_name, 
            user=self.database_username, 
            password=self.database_pass, 
            database='master'
        )
        self.cursor = self.conn.cursor()
    
    def disconnect(self):
        try:
            self.conn.close()
        except:
            print('Erro ao fechar conexao com banco.')
        
    def set_bot_id(self, bot_id):
        self.bot_id = bot_id

    def log_process(self, process_config, task_name, log_status, log_message):
        try:
            log_message = log_message.replace("'", "").replace('"', "")
            query = f"""
                    INSERT INTO {self.database_name}.dbo.logs
            (bot_id, processConfig, taskName, logStatus, logMessage, created_at)
            VALUES({self.bot_id}, '{process_config}', '{task_name}', '{log_status}', '{log_message}', getdate());

            """
            # print(query)
            self.cursor.execute(query)
            self.conn.commit()
        except:
            print('Erro log')

