import requests


# keyring.set_password("credential", "user", "pass")
class ApiViseu:
    def __init__(
        self,
        bot_name,
        environment_prod = False
    ):
        rpa_username = "rpa@grupoviseu.com.br"
        rpa_password = "Viseu@2021"
        self.username = rpa_username,
        self.password = rpa_password
        self.bot_name = bot_name
        self.bot_id = ""
        self.token = ""
        self.environment_prod = environment_prod
        self.data_corte = 2

        if (environment_prod == False):
            self.base_url = "https://74vcg38pt8.execute-api.us-east-1.amazonaws.com/staging"
            self.caminho_destino = f"C:\\BOT\\HapVida\\{self.bot_name}"

        if (environment_prod == True):
            self.base_url = "https://74vcg38pt8.execute-api.us-east-1.amazonaws.com/prod"
            # self.base_url = "54.160.73.14:3332"
            self.caminho_destino = "Z:"
            

        self.login_header = {}

        self.login()
        self.get_bot_id()

    def login(self)-> bool:
        try:
            url = f"{self.base_url}/users/login"
            
            payload = {'email': self.username, 'password': self.password}

            req = requests.post(url, json=payload).json()
            self.token = req['data']

            self.login_header = {'X-Authorization':self.token}
            return True

        except Exception as e:
            print(e)
            return False

    def get_bot_id(self): 
        url = f"{self.base_url}/bot/list/name/{self.bot_name}"
        req = requests.get(url, headers=self.login_header).json()
        self.bot_id = req['data']['id']

    def get_dealership_company_configs(self):
        url = f"{self.base_url}/concessionaria/config/list/company/{self.bot_id}"
        req = requests.get(url, headers=self.login_header).json()
        return req

    def createSapOrderMe21nMiro(self, api_matricula, api_dt_vencimento, api_mes_referencia, api_valor):

        url = f"{self.base_url}/hapvida/sap/menmiro/createOrder"
            
        payload = {
            'invoiceRegistration': api_matricula, 
            'invoiceDueDate': api_dt_vencimento,
            'invoiceReferenceMonth': api_mes_referencia,
            'invoiceValue': api_valor
        }

        req = requests.post(url, json=payload, headers=self.login_header)

        req_status_code = req.status_code
        req_status = "Sucesso"
        if(req_status_code == 500):
            req_status = "ErroSistema"
        if(req_status_code != 200 and req_status_code != 201 and req_status_code != 202):
            req_status = "ErroNegocio"
            
        req_json = req.json()
        
        return req_json, req_status
    
    def calcular_diferenca_datas(self, d1, d2):
        return int(d2)-int(d1)
        return abs((d2 - d1).days)   

    def set_data_corte(self, data_corte:int = 30) -> int:
        self.data_corte = data_corte

