import imaplib
import email
import os
import keyring

class Email:
    def __init__(
        self
    ):
        username = "vinicius.ortiz@grupoviseu.com.br"
        credential_key = "viseu_selenium_email"
        password = keyring.get_password(credential_key,username) 
        mail_server = 'imap.grupoviseu.com.br'
        self.username = username
        self.password = password
        self.mail_server = mail_server

    def connect(self):
        print(self.username)
        self.imap_server = imaplib.IMAP4(host=self.mail_server)
        self.imap_server.login(self.username, self.password)

    def list_folders(self):
        _, folders = self.imap_server.list()
        return folders
        
    def select_folder(self, inbox:str):
        self.imap_server.select(inbox)

    def list_emails_id(self, search_criteria:str = 'ALL'):
        charset = None  # All
        respose_code, message_numbers_raw = self.imap_server.search(charset, search_criteria)
        return message_numbers_raw[0].split()

    def select_email_by_id(self, email_message_id):
        _, msg = self.imap_server.fetch(email_message_id, '(RFC822)')
        message = email.message_from_bytes(msg[0][1])
        self.email_message = message
        
    def get_email_subject(self):
        return self.email_message["subject"]
    
    def get_email_message(self):
        message = ""
        if self.email_message.is_multipart():
            multipart_payload = self.email_message.get_payload()
            for sub_message in multipart_payload:
               
                message = message + f'{sub_message.get_payload()}\n'
        else:  
            message = message.get_payload()
        
        return message




 # print('== Email message =====')
        # # print(message)  # print FULL message
        # print('== Email details =====')
        # print(f'From: {message["from"]}')
        # print(f'To: {message["to"]}')
        # print(f'Cc: {message["cc"]}')
        # print(f'Bcc: {message["bcc"]}')
        # print(f'Urgency (1 highest 5 lowest): {message["x-priority"]}')
        # print(f'Object type: {type(message)}')
        # print(f'Content type: {message.get_content_type()}')
        # print(f'Content disposition: {message.get_content_disposition()}')
        # print(f'Multipart?: {message.is_multipart()}')