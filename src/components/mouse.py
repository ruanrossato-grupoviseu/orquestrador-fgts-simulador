from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement


def double_click_por_id(driver: WebDriver, id):
    elem = busca_elemento_por_id(driver, id)
    actionChains = ActionChains(driver)
    actionChains.double_click(elem).perform()

def double_click_por_xpath(driver: WebDriver, xpath):
    elem = busca_elemento_por_xpath(driver, xpath)
    actionChains = ActionChains(driver)
    actionChains.double_click(elem).perform()

def clicar_elemento(driver: WebDriver, elemento: WebElement):
    actionChains = ActionChains(driver)
    actionChains.click(elemento).perform()

def clicar_segurar_elemento(driver: WebDriver, elemento: WebElement):
    actionChains = ActionChains(driver)
    actionChains.click_and_hold(elemento).perform()
    
