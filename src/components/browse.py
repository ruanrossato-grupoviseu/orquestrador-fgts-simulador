from zipfile import error
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import json
import os
import subprocess
import time


def get_driver(profile:str = "")->WebDriver:

    # enable browser logging
    d = DesiredCapabilities.CHROME
    d['goog:loggingPrefs'] = { 'performance':'ALL' }
    options = webdriver.ChromeOptions()
    if profile != "":
        try:
            subprocess.call("TASKKILL /f  /IM  CHROME.EXE")
        except Exception:
            print(Exception)
            pass
        time.sleep(5)
        USERNAME = os.environ.get('USERNAME')
        
        options.add_argument(f"--user-data-dir=C:\\Users\\{USERNAME}\\AppData\\Local\\Google\\Chrome\\User Data")
        
        options.add_argument(fr'--profile-directory={profile}') #e.g. Profile 3

    # INICIALIZAR DRIVER
    settings = {
        "recentDestinations": [{
            "id": "Save as PDF",
            "origin": "local",
            "account": "",
        }],
        "selectedDestinationId": "Save as PDF",
        "version": 2
    }
        
    prefs = {
        # 'download.default_directory': "C:\\Users\\{USERNAME}\\Downloads",
        "profile.default_content_setting_values.automatic_downloads":1, 
        'printing.print_preview_sticky_settings.appState': json.dumps(settings)
    }

    options.add_experimental_option('prefs', prefs)
    options.add_argument('--kiosk-printing')

    
    driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=options, desired_capabilities=d)
    return driver

def openBrowser(webdriver: WebDriver, caminhoNavegador: str):
    driver = webdriver.Chrome(executable_path=caminhoNavegador)
    driver.set_page_load_timeout(120)
    return driver


def navegarSite(driver: WebDriver, url:str):
    driver.get(url)
    try:
        driver.maximize_window()
    except:
        pass