from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import WebDriver

def limpar_campo(driver: WebDriver, xpath: str, texto: str):
    driver.find_element(By.XPATH, xpath).clear() 