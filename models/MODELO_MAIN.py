try:
    import time
    from typing import List
    import os
    from datetime import date

    # IMPORT SELENIUM
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.webdriver.remote.webelement import WebElement

    # IMPORT FUNCOES
    from src.funcoes.autenticacao import login_linkedin
    from src.funcoes.linkedin import find_linkedin_url
    from src.funcoes.pdf import read_pdf
    from src.funcoes.classificador import create_document

    # IMPORT COMPONENTES
    from src.components.browse import navegarSite
    from src.components.lista import busca_lista_elementos_por_classe, clicar_lista_elemento_por_texto
    from src.components.file import delete_folder_files, wait_for_file_by_extension, file_exists
    from src.components.aguardar import aguardar_elemento_ser_clicavel_classe

    #IMPORT CLASS
    from src.models.Email import Email
    from src.models.Database import Database


    user_id = 2
    process_name = "Linkedin_Selenium"
    task_name = "Main.py"
    log_status = "Sucesso"

    database = Database(user_id=user_id, process_name=process_name)

    database.connect()


    log_message = "Iniciando processo"
    database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)

    USERNAME = os.environ.get('USERNAME')

    chrome_profile = f"C:\\Users\\{USERNAME}\\AppData\\Local\\Google\\Chrome\\User Data\\Default"

    options = webdriver.ChromeOptions()
    # options.add_argument(f"--user-data-dir=C:\\Users\\{USERNAME}\\AppData\\Local\\Google\\Chrome\\User Data")
    # options.add_argument(r'--profile-directory=Profile 3') #e.g. Profile 3
    # DECLARAR VARIAVEIS 
    site = "https://www.linkedin.com/login"
    caminhoBrowser = f"./driver/chromedriver.exe"
    caminho_downloads = f"C:\\Users\{USERNAME}\\Downloads"

    # keyring.set_password("credential", "user", "pass")

    # DECLARAR EMAIL BOX 
    emailInbox = "INBOX.RPA.LINKEDIN"

    # INICIALIZAR DRIVER
    driver = webdriver.Chrome(executable_path=caminhoBrowser, chrome_options=options)

    log_message = f"Abrindo chrome na url: {site}"
    database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)
    # ACESSAR SITE LINKEDIN 
    navegarSite(driver, site)

    # exit()
    print('Acessando site...')
    # REALIZAR LOGIN LINKEDIN
    log_message = f"Realizando login..."
    database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)
    try:
        elemento_classe = "search-global-typeahead__input"
        timeout = 5
        localizou = aguardar_elemento_ser_clicavel_classe(driver, elemento_classe, timeout)
    except:
        login_linkedin(driver)

    log_message = f"Login realizado!"
    database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)
    print('Abrindo email ...')

    log_message = f"Conectando no email..."
    database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)
    
    # CRIAR REFERENCIA PARA EMAIL
    email = Email()

    # CONECTAR NO EMAIL 
    email.connect()

    log_message = f"Selecionando caixa {emailInbox}"
    database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)
    # SELECIONAR INBOX EMAIL 
    email.select_folder(emailInbox)

    log_message = f"Obtendo lista de emails..."
    database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)
    # OBTER LISTA DE EMAILS 
    email_messages_id = email.list_emails_id()
     
    log_message = f"Percorrendo emails..."
    database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)
    # PERCORRER LISTA DE EMAIL ID 
    for email_message_id in email_messages_id:

        log_message = f"Processando email id: {email_message_id}"
        database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)

        # DELETAR ARQUIVOS DA PASTA DOWNLOADS
        delete_folder_files(caminho_downloads)

        # SELECIONAR EMAIL PELO ID 
        email.select_email_by_id(email_message_id)

        # OBTER EMAIL ASSUNTO 
        email_subject = email.get_email_subject()

        log_message = f"Email assunto: {email_subject}"
        database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)

        # OBTER EMAIL MENSAGEM 
        email_message = email.get_email_message()

        log_message = f"Email mensagem: {email_message}"
        database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)

        log_message = f"Buscando perfil do linkedin..."
        database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)

        # print("Mensagem: \n", email_message)
        # BUSCAR URL DO PERFIL A PARTIR DA MENSAGEM 
        linkedin_url_completa = find_linkedin_url(email_message)

        log_message = f"URL do linkedin obtida: {linkedin_url_completa}"
        database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)

        print("URL COMPLETA:", linkedin_url_completa)
        log_message = f"Navegando para URL"
        database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)
        # NAVEGAR URL LOCALIZADA 
        navegarSite(driver, linkedin_url_completa)

        # AGUARDAR SITE CARREGAR 
        time.sleep(5)

        log_message = f"Baixando PDF..."
        database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)

        # BUSCAR NOME 
        element_class = "text-heading-xlarge"
        person_name = busca_lista_elementos_por_classe(driver, element_class)[0].text
        print(person_name)
        # BUSCAR ELEMENTOS COM CLASSE DE AÇÃO 
        element = "pvs-profile-actions__action"
        list_profile_buttons: List[WebElement] = busca_lista_elementos_por_classe(driver, element)
        
        # CLICAR ELEMENTO CUJO VALOR É 'MAIS'
        element_text = 'Mais'
        clicar_lista_elemento_por_texto(list_profile_buttons, element_text)
        file_pdf = ""
        # TENTAR DOWNLOAD 10X
        for i in range(0, 10):
        
            time.sleep(1)
            # CLICAR ELEMENTO CUJO VALOR É 'Salvar como PDF'
            element_text = 'Salvar como PDF'
            elemento_localizado = clicar_lista_elemento_por_texto(list_profile_buttons, element_text)

            # SE NÃO LOCALIZOU ELEMENTO, ABRE O BOTÃO 'MAIS'
            if (elemento_localizado == False):
                element_text = 'Mais'
                clicar_lista_elemento_por_texto(list_profile_buttons, element_text)
                continue
                
            # PÓS CLICAR EM 'SALVAR COMO PDF', AGUARDAR DOWNLOAD POR 10 SEGUNDOS 
            file_timeout = 10
            file_extension = 'pdf'
            file_pdf = wait_for_file_by_extension(caminho_downloads, file_extension, file_timeout)

            # SE LOCALIZAR ARQUIVO, SAI DO LOOP 
            if(file_exists(file_pdf) == True):
                break

        # SE LOCALIZAR ARQUIVO, LÊ FORMATO PDF 
        if(file_exists(file_pdf) == True):
            log_message = f"PDF baixado com sucesso..."
            database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)
            
            log_message = f"Lendo PDF: {file_pdf}"
            database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)
            # LER ARQUIVO 
            pdf_text = read_pdf(file_pdf)

            log_message = f"Texto obtido: {pdf_text}"
            database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)

            # print(pdf_text)
            create_document(pdf_text, person_name)
            
        # break
    driver.quit()
    print('Finalizando processo...')
    exit(code=200)

except Exception as e:
    print('erro: ', str(e))
    log_message = str(e)
    log_status = "ERRO"
    database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)
    driver.quit()
    print('Finalizando processo...')
    exit(code=500)

