try:
    import time
    from typing import List
    import os
    from datetime import date

    # IMPORT SELENIUM
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.webdriver.remote.webelement import WebElement

    # IMPORT FUNCOES
    from src.funcoes.autenticacao import login_linkedin
    from src.funcoes.linkedin import find_linkedin_url
    from src.funcoes.pdf import read_pdf
    from src.funcoes.classificador import create_document
    import src.funcoes.windows_actions as wa

    # IMPORT COMPONENTES
    from src.components.browse import navegarSite
    from src.components.lista import busca_lista_elementos_por_classe, clicar_lista_elemento_por_texto
    from src.components.file import delete_folder_files, wait_for_file_by_extension, file_exists
    from src.components.aguardar import aguardar_elemento_ser_clicavel_classe

    #IMPORT CLASS
    from src.models.Email import Email
    from src.models.Database import Database


    user_id = 2
    process_name = ""
    task_name = ""
    log_status = "Sucesso"

    database = Database(user_id=user_id, process_name=process_name)

    database.connect()


    log_message = "Iniciando processo"
    database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)

    USERNAME = os.environ.get('USERNAME')

    chrome_profile = f"C:\\Users\\{USERNAME}\\AppData\\Local\\Google\\Chrome\\User Data\\Default"

    options = webdriver.ChromeOptions()
    # options.add_argument(f"--user-data-dir=C:\\Users\\{USERNAME}\\AppData\\Local\\Google\\Chrome\\User Data")
    # options.add_argument(r'--profile-directory=Profile 3') #e.g. Profile 3
    # DECLARAR VARIAVEIS 
    site = ""
    caminhoBrowser = f"./driver/chromedriver.exe"
    caminho_downloads = f"C:\\Users\{USERNAME}\\Downloads"

    # keyring.set_password("credential", "user", "pass")

    # DECLARAR EMAIL BOX 
    emailInbox = "INBOX.RPA.LINKEDIN"

    # INICIALIZAR DRIVER
    driver = webdriver.Chrome(executable_path=caminhoBrowser, chrome_options=options)

    log_message = f"Abrindo chrome na url: {site}"
    database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)
    # ACESSAR SITE LINKEDIN 
    navegarSite(driver, site)

    
            
        # break
    driver.quit()
    print('Finalizando processo...')
    exit(code=200)

except Exception as e:
    print('erro: ', str(e))
    log_message = str(e)
    log_status = "ERRO"
    database.log_process(task_name=task_name, log_status=log_status, log_message=log_message)
    driver.quit()
    print('Finalizando processo...')
    exit(code=500)

