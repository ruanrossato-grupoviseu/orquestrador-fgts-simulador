from pymongo import MongoClient
from time import sleep
import urllib3
import sys, json

def connectToMongo(dev = False):
    
    client = MongoClient("mongodb+srv://chatbot:senhasegurachatbotrcincoviseu@r5-fgts.lhryy.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
    database = client["simulation"]

    simulationRequests = database["simulationRequests"]
    simulationResults = database["simulationResults"]

    return simulationResults,simulationRequests



from datetime import date, datetime
def json_serial(obj):

    if isinstance(obj, (datetime, date)):
        return obj.__str__()
    raise TypeError ("Type %s not serializable" % type(obj))

from main import startBotC6, startBotSafra

simulationResults, simulationRequests = connectToMongo()

animation = "|/-\\"
idx = 0

while(True):
    print(f"Procurando por requisicoes de simulacao {animation[idx % len(animation)]}", end="\r")
    idx += 1           
    request  = simulationRequests.find_one()
    if(request):
        hasSimulation = False
        simulationList =[]
        
        print(request)
        try:
            raise Exception("bypass safra")
            simulacaoSafra = startBotSafra(userInfo = request, simulation = request["simulation"])
            hasSimulation = True
            simulationList.append({
                                    "bank":"Banco Safra",
                                    "simulation":simulacaoSafra
                                })
        except Exception as err:
            simulacaoSafra = None
            print("Error: ",err)

        try:
            
            simulacaoC6 = startBotC6(userInfo = request, simulation = request["simulation"])
            hasSimulation = True
            simulationList.append({
                                        "bank":"Banco C6",
                                        "simulation":simulacaoC6
                                    })
        except Exception as err:
            simulacaoC6 = None
            print("Error: ",err)

        
        simulationInfo = {
                            "phoneNumber":request["phoneNumber"],
                            "datetime": datetime.today(),
                            "hasSimulation":hasSimulation,
                            "simulation":simulationList
                        }
        simulationResults.insert_one(simulationInfo)
        simulationInfo.pop("_id")
        ''''''
        url = "https://mg8z2p7ewk.execute-api.sa-east-1.amazonaws.com/dev/simulation-notification"
        try:
            headers = {
            'Content-Type': 'application/json'
            }
            http = urllib3.PoolManager()
            print(simulationInfo)
            response = http.request("POST",
                        url,
                        body=json.dumps(simulationInfo,default=json_serial,ensure_ascii=False),
                        headers=headers
                        )
            print(response.data)
            print(response.status)
            simulationRequests.delete_one({"phoneNumber":request["phoneNumber"]})

        except Exception as e:
            print("Failed to notify chatbot",e) 

    sleep(1)
